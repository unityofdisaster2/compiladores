E->E+T|E-T|T;
T->T*F|T/F|F;
F->(E)|num;
 token: 10 lexema: E
 token: 20 lexema: ->
 token: 10 lexema: E
 token: 10 lexema: +
 token: 10 lexema: T
 token: 40 lexema: |
 token: 10 lexema: E
 token: 10 lexema: -
 token: 10 lexema: T
 token: 40 lexema: |
 token: 10 lexema: T
 token: 30 lexema: ;
 token: 10 lexema: T
 token: 20 lexema: ->
 token: 10 lexema: T
 token: 10 lexema: *
 token: 10 lexema: F
 token: 40 lexema: |
 token: 10 lexema: T
 token: 10 lexema: /
 token: 10 lexema: F
 token: 40 lexema: |
 token: 10 lexema: F
 token: 30 lexema: ;
 token: 10 lexema: F
 token: 20 lexema: ->
 token: 10 lexema: (
 token: 10 lexema: E
 token: 10 lexema: )
 token: 40 lexema: |
 token: 10 lexema: num
 token: 30 lexema: ;
 token: 1000 lexema: ;
LR1------
Ep-> . E			[ $   ] 
^^Cerradura:
Ep-> . E			[ $   ] 
E-> . E + T			[ $  +  -   ] 
E-> . E - T			[ $  +  -   ] 
E-> . T			[ $  +  -   ] 
T-> . T * F			[ $  *  +  -  /   ] 
T-> . T / F			[ $  *  +  -  /   ] 
T-> . F			[ $  *  +  -  /   ] 
F-> . ( E )			[ $  *  +  -  /   ] 
F-> . num			[ $  *  +  -  /   ] 
------------------------------------------1
Ep-> . E			[ $   ] 
E-> . E + T			[ $  +  -   ] 
E-> . E - T			[ $  +  -   ] 
E-> . T			[ $  +  -   ] 
T-> . T * F			[ $  *  +  -  /   ] 
T-> . T / F			[ $  *  +  -  /   ] 
T-> . F			[ $  *  +  -  /   ] 
F-> . ( E )			[ $  *  +  -  /   ] 
F-> . num			[ $  *  +  -  /   ] 
------------------------------------------
Con E
Ep-> E .			[ $   ] 
E-> E . + T			[ $  +  -   ] 
E-> E . - T			[ $  +  -   ] 
^^Cerradura:
al 2
Con T
E-> T .			[ $  +  -   ] 
T-> T . * F			[ $  *  +  -  /   ] 
T-> T . / F			[ $  *  +  -  /   ] 
^^Cerradura:
al 3
Con F
T-> F .			[ $  *  +  -  /   ] 
^^Cerradura:
al 4
Con (
F-> ( . E )			[ $  *  +  -  /   ] 
^^Cerradura:
al 5
Antes
Con num
F-> num .			[ $  *  +  -  /   ] 
^^Cerradura:
al 6
Antes
------------------------------------------6
Ep-> E .			[ $   ] 
E-> E . + T			[ $  +  -   ] 
E-> E . - T			[ $  +  -   ] 
------------------------------------------
Con +
E-> E + . T			[ $  +  -   ] 
^^Cerradura:
al 7
Antes
Con -
E-> E - . T			[ $  +  -   ] 
^^Cerradura:
al 8
Antes
------------------------------------------8
E-> T .			[ $  +  -   ] 
T-> T . * F			[ $  *  +  -  /   ] 
T-> T . / F			[ $  *  +  -  /   ] 
------------------------------------------
Con *
T-> T * . F			[ $  *  +  -  /   ] 
^^Cerradura:
al 9
Antes
Con /
T-> T / . F			[ $  *  +  -  /   ] 
^^Cerradura:
al 10
Antes
------------------------------------------10
T-> F .			[ $  *  +  -  /   ] 
------------------------------------------
------------------------------------------10
F-> ( . E )			[ $  *  +  -  /   ] 
E-> . E + T			[ )  +  -   ] 
E-> . E - T			[ )  +  -   ] 
E-> . T			[ )  +  -   ] 
T-> . T * F			[ )  *  +  -  /   ] 
T-> . T / F			[ )  *  +  -  /   ] 
T-> . F			[ )  *  +  -  /   ] 
F-> . ( E )			[ )  *  +  -  /   ] 
F-> . num			[ )  *  +  -  /   ] 
------------------------------------------
Con E
F-> ( E . )			[ $  *  +  -  /   ] 
E-> E . + T			[ )  +  -   ] 
E-> E . - T			[ )  +  -   ] 
^^Cerradura:
al 11
Con T
E-> T .			[ )  +  -   ] 
T-> T . * F			[ )  *  +  -  /   ] 
T-> T . / F			[ )  *  +  -  /   ] 
^^Cerradura:
al 12
Con F
T-> F .			[ )  *  +  -  /   ] 
^^Cerradura:
al 13
Con (
F-> ( . E )			[ )  *  +  -  /   ] 
^^Cerradura:
al 14
Antes
Con num
F-> num .			[ )  *  +  -  /   ] 
^^Cerradura:
al 15
Antes
------------------------------------------15
F-> num .			[ $  *  +  -  /   ] 
------------------------------------------
------------------------------------------15
E-> E + . T			[ $  +  -   ] 
T-> . T * F			[ $  *  +  -  /   ] 
T-> . T / F			[ $  *  +  -  /   ] 
T-> . F			[ $  *  +  -  /   ] 
F-> . ( E )			[ $  *  +  -  /   ] 
F-> . num			[ $  *  +  -  /   ] 
------------------------------------------
Con T
E-> E + T .			[ $  +  -   ] 
T-> T . * F			[ $  *  +  -  /   ] 
T-> T . / F			[ $  *  +  -  /   ] 
^^Cerradura:
al 16
Con F
T-> F .			[ $  *  +  -  /   ] 
^^Cerradura:
Con (
F-> ( . E )			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
Con num
F-> num .			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------16
E-> E - . T			[ $  +  -   ] 
T-> . T * F			[ $  *  +  -  /   ] 
T-> . T / F			[ $  *  +  -  /   ] 
T-> . F			[ $  *  +  -  /   ] 
F-> . ( E )			[ $  *  +  -  /   ] 
F-> . num			[ $  *  +  -  /   ] 
------------------------------------------
Con T
E-> E - T .			[ $  +  -   ] 
T-> T . * F			[ $  *  +  -  /   ] 
T-> T . / F			[ $  *  +  -  /   ] 
^^Cerradura:
al 17
Con F
T-> F .			[ $  *  +  -  /   ] 
^^Cerradura:
Con (
F-> ( . E )			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
Con num
F-> num .			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------17
T-> T * . F			[ $  *  +  -  /   ] 
F-> . ( E )			[ $  *  +  -  /   ] 
F-> . num			[ $  *  +  -  /   ] 
------------------------------------------
Con F
T-> T * F .			[ $  *  +  -  /   ] 
^^Cerradura:
al 18
Con (
F-> ( . E )			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
Con num
F-> num .			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------18
T-> T / . F			[ $  *  +  -  /   ] 
F-> . ( E )			[ $  *  +  -  /   ] 
F-> . num			[ $  *  +  -  /   ] 
------------------------------------------
Con F
T-> T / F .			[ $  *  +  -  /   ] 
^^Cerradura:
al 19
Con (
F-> ( . E )			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
Con num
F-> num .			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------19
F-> ( E . )			[ $  *  +  -  /   ] 
E-> E . + T			[ )  +  -   ] 
E-> E . - T			[ )  +  -   ] 
------------------------------------------
Con )
F-> ( E ) .			[ $  *  +  -  /   ] 
^^Cerradura:
al 20
Antes
Con +
E-> E + . T			[ )  +  -   ] 
^^Cerradura:
al 21
Antes
Con -
E-> E - . T			[ )  +  -   ] 
^^Cerradura:
al 22
Antes
------------------------------------------22
E-> T .			[ )  +  -   ] 
T-> T . * F			[ )  *  +  -  /   ] 
T-> T . / F			[ )  *  +  -  /   ] 
------------------------------------------
Con *
T-> T * . F			[ )  *  +  -  /   ] 
^^Cerradura:
al 23
Antes
Con /
T-> T / . F			[ )  *  +  -  /   ] 
^^Cerradura:
al 24
Antes
------------------------------------------24
T-> F .			[ )  *  +  -  /   ] 
------------------------------------------
------------------------------------------24
F-> ( . E )			[ )  *  +  -  /   ] 
E-> . E + T			[ )  +  -   ] 
E-> . E - T			[ )  +  -   ] 
E-> . T			[ )  +  -   ] 
T-> . T * F			[ )  *  +  -  /   ] 
T-> . T / F			[ )  *  +  -  /   ] 
T-> . F			[ )  *  +  -  /   ] 
F-> . ( E )			[ )  *  +  -  /   ] 
F-> . num			[ )  *  +  -  /   ] 
------------------------------------------
Con E
F-> ( E . )			[ )  *  +  -  /   ] 
E-> E . + T			[ )  +  -   ] 
E-> E . - T			[ )  +  -   ] 
^^Cerradura:
al 25
Con T
E-> T .			[ )  +  -   ] 
T-> T . * F			[ )  *  +  -  /   ] 
T-> T . / F			[ )  *  +  -  /   ] 
^^Cerradura:
Con F
T-> F .			[ )  *  +  -  /   ] 
^^Cerradura:
Con (
F-> ( . E )			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
Con num
F-> num .			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------25
F-> num .			[ )  *  +  -  /   ] 
------------------------------------------
------------------------------------------25
E-> E + T .			[ $  +  -   ] 
T-> T . * F			[ $  *  +  -  /   ] 
T-> T . / F			[ $  *  +  -  /   ] 
------------------------------------------
Con *
T-> T * . F			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
Con /
T-> T / . F			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------25
E-> E - T .			[ $  +  -   ] 
T-> T . * F			[ $  *  +  -  /   ] 
T-> T . / F			[ $  *  +  -  /   ] 
------------------------------------------
Con *
T-> T * . F			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
Con /
T-> T / . F			[ $  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------25
T-> T * F .			[ $  *  +  -  /   ] 
------------------------------------------
------------------------------------------25
T-> T / F .			[ $  *  +  -  /   ] 
------------------------------------------
------------------------------------------25
F-> ( E ) .			[ $  *  +  -  /   ] 
------------------------------------------
------------------------------------------25
E-> E + . T			[ )  +  -   ] 
T-> . T * F			[ )  *  +  -  /   ] 
T-> . T / F			[ )  *  +  -  /   ] 
T-> . F			[ )  *  +  -  /   ] 
F-> . ( E )			[ )  *  +  -  /   ] 
F-> . num			[ )  *  +  -  /   ] 
------------------------------------------
Con T
E-> E + T .			[ )  +  -   ] 
T-> T . * F			[ )  *  +  -  /   ] 
T-> T . / F			[ )  *  +  -  /   ] 
^^Cerradura:
al 26
Con F
T-> F .			[ )  *  +  -  /   ] 
^^Cerradura:
Con (
F-> ( . E )			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
Con num
F-> num .			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------26
E-> E - . T			[ )  +  -   ] 
T-> . T * F			[ )  *  +  -  /   ] 
T-> . T / F			[ )  *  +  -  /   ] 
T-> . F			[ )  *  +  -  /   ] 
F-> . ( E )			[ )  *  +  -  /   ] 
F-> . num			[ )  *  +  -  /   ] 
------------------------------------------
Con T
E-> E - T .			[ )  +  -   ] 
T-> T . * F			[ )  *  +  -  /   ] 
T-> T . / F			[ )  *  +  -  /   ] 
^^Cerradura:
al 27
Con F
T-> F .			[ )  *  +  -  /   ] 
^^Cerradura:
Con (
F-> ( . E )			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
Con num
F-> num .			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------27
T-> T * . F			[ )  *  +  -  /   ] 
F-> . ( E )			[ )  *  +  -  /   ] 
F-> . num			[ )  *  +  -  /   ] 
------------------------------------------
Con F
T-> T * F .			[ )  *  +  -  /   ] 
^^Cerradura:
al 28
Con (
F-> ( . E )			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
Con num
F-> num .			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------28
T-> T / . F			[ )  *  +  -  /   ] 
F-> . ( E )			[ )  *  +  -  /   ] 
F-> . num			[ )  *  +  -  /   ] 
------------------------------------------
Con F
T-> T / F .			[ )  *  +  -  /   ] 
^^Cerradura:
al 29
Con (
F-> ( . E )			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
Con num
F-> num .			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------29
F-> ( E . )			[ )  *  +  -  /   ] 
E-> E . + T			[ )  +  -   ] 
E-> E . - T			[ )  +  -   ] 
------------------------------------------
Con )
F-> ( E ) .			[ )  *  +  -  /   ] 
^^Cerradura:
al 30
Antes
Con +
E-> E + . T			[ )  +  -   ] 
^^Cerradura:
Antes
Con -
E-> E - . T			[ )  +  -   ] 
^^Cerradura:
Antes
------------------------------------------30
E-> E + T .			[ )  +  -   ] 
T-> T . * F			[ )  *  +  -  /   ] 
T-> T . / F			[ )  *  +  -  /   ] 
------------------------------------------
Con *
T-> T * . F			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
Con /
T-> T / . F			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------30
E-> E - T .			[ )  +  -   ] 
T-> T . * F			[ )  *  +  -  /   ] 
T-> T . / F			[ )  *  +  -  /   ] 
------------------------------------------
Con *
T-> T * . F			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
Con /
T-> T / . F			[ )  *  +  -  /   ] 
^^Cerradura:
Antes
------------------------------------------30
T-> T * F .			[ )  *  +  -  /   ] 
------------------------------------------
------------------------------------------30
T-> T / F .			[ )  *  +  -  /   ] 
------------------------------------------
------------------------------------------30
F-> ( E ) .			[ )  *  +  -  /   ] 
------------------------------------------
+	-	*	/	(	)	num	$	E	T	F	
				d4		d5		1	2	3	
d6	d7						acc				
r3	r3	d8	d9				r3				
r6	r6	r6	r6				r6				
				d13		d14		10	11	12	
r8	r8	r8	r8				r8				
				d4		d5			15	3	
				d4		d5			16	3	
				d4		d5				17	
				d4		d5				18	
d20	d21				d19						
r3	r3	d22	d23		r3						
r6	r6	r6	r6		r6						
				d13		d14		24	11	12	
r8	r8	r8	r8		r8						
r1	r1	d8	d9				r1				
r2	r2	d8	d9				r2				
r4	r4	r4	r4				r4				
r5	r5	r5	r5				r5				
r7	r7	r7	r7				r7				
				d13		d14			25	12	
				d13		d14			26	12	
				d13		d14				27	
				d13		d14				28	
d20	d21				d29						
r1	r1	d22	d23		r1						
r2	r2	d22	d23		r2						
r4	r4	r4	r4		r4						
r5	r5	r5	r5		r5						
r7	r7	r7	r7		r7						
Inicio
	[0] []   cadena:[(, num, +, num, ), $]
	Accion d4
	[0, 4] [(]   cadena:[num, +, num, ), $]
	Accion d14
	[0, 4, 14] [(, num]   cadena:[+, num, ), $]
	Accion r8
	[0, 4, 12] [(, F]   cadena:[+, num, ), $]
	Accion r6
	[0, 4, 11] [(, T]   cadena:[+, num, ), $]
	Accion r3
	[0, 4, 10] [(, E]   cadena:[+, num, ), $]
	Accion d20
	[0, 4, 10, 20] [(, E, +]   cadena:[num, ), $]
	Accion d14
	[0, 4, 10, 20, 14] [(, E, +, num]   cadena:[), $]
	Accion r8
	[0, 4, 10, 20, 12] [(, E, +, F]   cadena:[), $]
	Accion r6
	[0, 4, 10, 20, 25] [(, E, +, T]   cadena:[), $]
	Accion r1
	[0, 4, 10] [(, E]   cadena:[), $]
	Accion d19
	[0, 4, 10, 19] [(, E, )]   cadena:[$]
	Accion r7
	[0, 3] [F]   cadena:[$]
	Accion r6
	[0, 2] [T]   cadena:[$]
	Accion r3
	[0, 1] [E]   cadena:[$]
Siiii
