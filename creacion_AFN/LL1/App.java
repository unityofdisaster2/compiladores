import java.util.HashSet;
import java.util.Set;
import java.util.LinkedList;

//considerar epsilon como #

public class App{
    public static void main(String[] args) {
        ListaDeListas ll = new ListaDeListas();

        ll.nuevaRegla("E");
        ll.agregarNodo("T");
        ll.agregarNodo("E'");
        ll.terminarReglaActual();

        ll.nuevaRegla("E'");
        ll.agregarNodo("+");
        ll.agregarNodo("T");
        ll.agregarNodo("E'");
        ll.terminarReglaActual();

        ll.nuevaRegla("E'");
        ll.agregarNodo("#");
        ll.terminarReglaActual();

        ll.nuevaRegla("T");
        ll.agregarNodo("F");
        ll.agregarNodo("T'");
        ll.terminarReglaActual();


        ll.nuevaRegla("T'");
        ll.agregarNodo("*");
        ll.agregarNodo("F");
        ll.agregarNodo("T'");
        ll.terminarReglaActual();

        ll.nuevaRegla("T'");
        ll.agregarNodo("#");
        ll.terminarReglaActual();
       

        ll.nuevaRegla("F");
        ll.agregarNodo("(");
        ll.agregarNodo("E");
        ll.agregarNodo(")");
        ll.terminarReglaActual();

        ll.nuevaRegla("F");
        ll.agregarNodo("id");
        ll.terminarReglaActual();
       

        ll.addInicial("E");
        
        ll.mostrarMapa();

      /*
        ll.nuevaRegla("E");
        ll.agregarNodo("T");
        ll.agregarNodo("R");
        ll.terminarReglaActual();

        ll.nuevaRegla("R");
        ll.agregarNodo("+");
        ll.agregarNodo("T");
        ll.agregarNodo("R");
        ll.terminarReglaActual();

        ll.nuevaRegla("R");
        ll.agregarNodo("#");
        ll.terminarReglaActual();

        ll.nuevaRegla("T");
        ll.agregarNodo("F");
        ll.agregarNodo("Y");
        ll.terminarReglaActual();


        ll.nuevaRegla("Y");
        ll.agregarNodo("*");
        ll.agregarNodo("F");
        ll.agregarNodo("Y");
        ll.terminarReglaActual();

        ll.nuevaRegla("Y");
        ll.agregarNodo("#");
        ll.terminarReglaActual();
       

        ll.nuevaRegla("F");
        ll.agregarNodo("(");
        ll.agregarNodo("E");
        ll.agregarNodo(")");
        ll.terminarReglaActual();

        ll.nuevaRegla("F");
        ll.agregarNodo("i");
        ll.terminarReglaActual();
       

        ll.addInicial("E");
        
        ll.mostrarMapa();
*/

        /*
        ll.addInicial("Ep");        
        ll.nuevaRegla("Ep");
        ll.agregarNodo("+");
        ll.agregarNodo("T");
        ll.agregarNodo("Ep");
        ll.terminarReglaActual();
        ll.nuevaRegla("Ep");
        ll.agregarNodo("-");
        ll.agregarNodo("T");
        ll.agregarNodo("Ep");
        ll.terminarReglaActual();
        ll.nuevaRegla("Ep");
        ll.agregarNodo("#");
        ll.terminarReglaActual();
        ll.mostrarMapa();
        ll.nuevaRegla("F");
        ll.agregarNodo("(");
        ll.agregarNodo("Ep");
        ll.agregarNodo(")");
        ll.terminarReglaActual();
        ll.nuevaRegla("F");
        ll.agregarNodo("SIM");
        ll.terminarReglaActual();
        ll.mostrarMapa();
*/

        //prueba de first y follow
        for(String term:ll.getNoTerminales()){
            System.out.println("\nFirst de "+term);
            LinkedList a = new LinkedList();
            a.add(term);
            Set<String> primeros;
            primeros = ll.first(a);
    
            for(String s:primeros){
                System.out.print(s+" ");
            }
            
            System.out.println("\nFollow de "+term);
            primeros = ll.follow(term);
    
            for(String s:primeros){
                System.out.print(s+" ");
            }
        }       

        ll.mostrarTabla();
        LinkedList<String> cadena = new LinkedList<>();
        cadena.add("(");
        cadena.add("id");
        cadena.add(")");

        LinkedList<String> aux=new LinkedList<>();
        aux.add("E");
        aux.add("$");

        LinkedList<String> aux2= new LinkedList<>();
	aux2.add("(");
    aux2.add("id");
    aux2.add("+");
    aux2.add("id");
	aux2.add(")");
	aux2.add("*");
	aux2.add("(");
    aux2.add("id");
	aux2.add(")");
    
        LinkedList<String> aux3 = new LinkedList<>();
	System.out.println("\n:\n"+ll.getMovimiento("T","id"));
        System.out.println("\nInicio:\n"+aux+" "+aux2);
        while(!aux.isEmpty()){
	    if(aux.getFirst()=="$"){
            if(aux2.isEmpty()){
                System.out.println("Si");
            }
            else{
                System.out.println("No");
            }
		    break;
	    }
	    if(aux2.isEmpty()){
		aux3.clear();
                aux3.addAll(ll.getMovimiento(aux.getFirst(),"$"));
		//System.out.println("aux "+aux.getFirst()+" aux2 $ Agregando "+aux3);
                aux.removeFirst();
                if(aux3==null){                
                    System.out.println("No");
                    break;
                }
            	aux3.addAll(aux);
            	aux.clear();
	    	aux.addAll(aux3);
	    }
            if(aux.getFirst()=="#"){
                aux.removeFirst();
            }else if(aux.getFirst()==aux2.getFirst()){
                aux.removeFirst();
                aux2.removeFirst();
            }
            else{
		        aux3.clear();
                aux3.addAll(ll.getMovimiento(aux.getFirst(),aux2.getFirst()));
		//System.out.println("aux "+aux.getFirst()+" aux2 "+aux2.getFirst()+" Agregando "+aux3);
                aux.removeFirst();
                if(aux3==null){                
                    System.out.println("No");
                    break;
                }
            	aux3.addAll(aux);
            	aux.clear();
	    	aux.addAll(aux3);
            }
            System.out.println(aux+" "+aux2);
        }





    }
}
