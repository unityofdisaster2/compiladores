import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;

public class ListaDeListas{
    private String inicial;
    private HashMap<String,LinkedList<LinkedList<String>>> mapaReglas;
    private String izquierdo;
    private LinkedList<String> tempRegla;
    private LinkedList<LinkedList<String>> tempListaReglas;
    private HashSet<String> terminales;
    private HashSet<String> noTerminales;
    private HashMap<String,HashMap<String,LinkedList<String>>> parseTable;
    
    public ListaDeListas(){

        mapaReglas = new HashMap<>();
        tempRegla = new LinkedList<>();
        tempListaReglas = new LinkedList<>();
        terminales = new HashSet<>();
        noTerminales = new HashSet<>();
        parseTable = new HashMap<>();

    }

    public void addInicial(String inicial){
        this.inicial = inicial;
        tempRegla = new LinkedList<>();
    }

    /**
     * Metodo para crear una nueva regla para la gramatica 
     * @param izquierdo Se recibe como parametro la parte izquierda de la regla y se inicializa
     * un arreglo de reglas temporales 
     */
    public void nuevaRegla(String izquierdo){

        if(!mapaReglas.containsKey(izquierdo)){
            mapaReglas.put(izquierdo,new LinkedList<>());
            noTerminales.add(izquierdo);
            if(terminales.contains(izquierdo))
                terminales.remove(izquierdo);
            this.izquierdo = izquierdo;
            tempRegla = new LinkedList<>();    
        }else{
            tempRegla = new LinkedList<>();
        }
    }

    public void terminarReglaActual(){
        mapaReglas.get(this.izquierdo).add(tempRegla);
    }

    public HashMap<String,LinkedList<LinkedList<String>>> getMapa(){
        return this.mapaReglas;
    }

    public HashSet<String> getNoTerminales(){
        return noTerminales;
    }

    public LinkedList<String> getRegla(String a,String b){
        LinkedList<LinkedList<String>> reglas = mapaReglas.get(a);
        for(LinkedList<String> regla:reglas){
            if(regla.get(0)==b){
                return regla;
            }
        }
        
        return reglas.get(0);
    }
    
    public LinkedList<String> getMovimiento(String noTerm,String term){
        return parseTable.get(noTerm).get(term);
    }

    public void mostrarTabla(){
        for(String a:noTerminales){ //recorre los terminales
            parseTable.put(a,new HashMap<>());
            LinkedList lista = new LinkedList();
            lista.add(a);
            Set<String> primeros = first(lista); //obtengo first del no terminal
            for(String b:terminales){ //recorre los no terminales
                if(b!="#" && primeros.contains(b)){
                    HashMap<String,LinkedList<String>> aux = parseTable.get(a);
                    aux.put(b,getRegla(a,b));            
                }
            }
            if(primeros.contains("#")){
                Set<String> siguiente = follow(a); //obtengo first del no terminal

                for(String b:terminales){ //recorre los no terminales
                    HashMap<String,LinkedList<String>> aux = parseTable.get(a);
                    LinkedList<String> aux2 = new LinkedList<>();
                    aux2.add("#");
                    if(siguiente.contains(b)){
                            aux.put(b,aux2);            
                    }
                }

                if(siguiente.contains("$")){
                    HashMap<String,LinkedList<String>> aux = parseTable.get(a);
                    LinkedList<String> aux2 = new LinkedList<>();
                    aux2.add("#");
                    aux.put("$",aux2);
                }
            }
        }


        //imprimir

        System.out.print("\nRegla\t");
        for(String b:terminales){ //recorre los no terminales
            if(b!="#"){
                System.out.print(b+"\t\t");
            }
        }
        System.out.print("$\t\t");
        for(String a:noTerminales){
        System.out.print("\n"+a+"\t");
            for(String b:terminales){
                
                if(b!="#"){
                    if(parseTable.get(a).get(b)!=null){
                        System.out.print(parseTable.get(a).get(b)+"\t");
                    }else{
                        System.out.print("x\t\t");
                    }
                } 
            }

            if(parseTable.get(a).get('$')!=null){
                System.out.print(parseTable.get(a).get("$")+"\t");
            }else{
                System.out.print("x\t\t");
            }

        }
    }

    

    public void mostrarMapa(){
        String aux = "";
        System.out.println("tamano mapa: "+mapaReglas.size());
        for(String s: mapaReglas.keySet()){
            System.out.print(s+"->");
            for(LinkedList<String> lin: mapaReglas.get(s)){
                for(String regla: lin){
                    aux +=regla;
                }
                System.out.print(aux);
                aux = "";
                System.out.print("|");
            }
            System.out.println("");
            aux = "";
        }

        System.out.println();
    }

    /**
     * En este metodo se agregaran uno a uno los nodos correspondientes a cada regla
     * @param s se recibe como argumento un caracter que representa un terminal o un no terminal
     * que pasara a formar un nodo
     */
    public void agregarNodo(String s){
        this.tempRegla.add(s);
        if(!noTerminales.contains(s)) //si no es un terminal
            this.terminales.add(s);
    }


    public Set<String> first(LinkedList<String> R){
        Set<String> conjunto = new HashSet<String>();

        if(terminales.contains(R.get(0))){             //si es terminal el primer elemento
            conjunto.add(R.get(0));                     //se agrega el caracter al conjunto
            return conjunto;
        }

        for(LinkedList<String> lin: mapaReglas.get(R.get(0))){ //cada regla separada
            conjunto.addAll(first(lin));
        }

        if(conjunto.contains("#") && R.size()>1){
            conjunto.remove("#");
            conjunto.addAll(first((LinkedList<String>)R.subList(1,R.size())));
        } 
        return conjunto;
    }

    public Set<String> follow (String R){ //R es la regla a la que se quiere aplicar
        Set<String> conjunto = new HashSet<String>();
        if(R==inicial){                        //si R es inicial
            conjunto.add("$");
        }
        //System.out.println("\nFollow de "+R+":");

        for(String s: mapaReglas.keySet()){ //busca en cada regla
            for(LinkedList<String> lin: mapaReglas.get(s)){
                
                if(lin.contains(R)){ //si R esta del lado derecho de la regla
                    
                    int inicio=lin.indexOf(R);                                                  //busca en que parte esta

                    LinkedList<String> aux = new LinkedList<>();
                    aux.addAll(lin.subList(inicio, lin.size()));
                    
                    if(aux.size()==1){                      //no tiene nada despues

                        if(R!=s){                               //si no es el mismo que ya se esta evaluando
                            conjunto.addAll(follow(s));                    
                        }
                    }else{ //si tiene algo despues
                        
                        LinkedList<String> aux2 = new LinkedList<>();
                        aux2.addAll(aux.subList(1, aux.size()));
                        conjunto.addAll(first(aux2));              //saca first
                        if(conjunto.contains("#")){                 //si tiene epsilon, se hace follow al no terminal
                            conjunto.remove("#");                   //se quita epsilon
                            conjunto.addAll(follow(s));
                        }
                    } 
                }
            }
        }
        return conjunto;
    }

    
}


/*
E -> TE'
E' -> +TE' | -TE' | Epsilon
T -> FT'
T'-> *FT' | /FT' | Epsilon
F -> (E) | Epsilon 
*/