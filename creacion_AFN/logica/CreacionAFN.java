package logica;
import java.util.*;
import java.io.*;

public class CreacionAFN{
    public static void main(String[] args){
        
        AFN a = new AFN(); //se crea un AFN
        AFN b = new AFN(); //se crea un AFN
        AFN c = new AFN(); //se crea un AFN
        AFN d = new AFN(); //se crea un AFN





        //prueba 1
/*
        a.basico('a');     //se hace uno basico
        a.Question();

        b.basico('b');
        b.cerraduraPositiva();

        c.basico('c');

        b.unir(c);
        
        d.basico('d');
        d.cerraduraEstrella();

        a.concatenar(b);
        a.concatenar(d);
        
        
        //a.cerraduraEstrella();
        //a.cerraduraPositiva();
        a.imprimirTransicionesAFN();
       */ 

        /* prueba 2
        a.basico('c');
        b.basico('d');
        a.unir(b);
        a.imprimirTransicionesAFN();
        */


        //prueba ejercicio del maestro :V
/*
        
        AFN token10 = new AFN(); //se crea un AFN
        AFN token20 = new AFN(); //se crea un AFN
        AFN token30 = new AFN(); //se crea un AFN
        AFN token40 = new AFN(); //se crea un AFN

        //token 10
        token10.basico('s');
        token10.Question();
        a.basico('d');
        a.cerraduraPositiva();
        token10.concatenar(a);
        token10.setToken(10);

        //token 20
        a = new AFN();
        b = new AFN();
        c = new AFN();


        token20.basico('s');
        token20.Question();
        a.basico('d');
        a.cerraduraPositiva();
        b.basico('.');
        c.basico('d');
        c.cerraduraPositiva();
        token20.concatenar(a);
        token20.concatenar(b);
        token20.concatenar(c);
        token20.setToken(20);


        //token30
        a = new AFN();
        b = new AFN();
        token30.basico('l');
        a.basico('l');
        b.basico('d');
        a.unir(b);
        a.cerraduraEstrella();
        token30.concatenar(a);
        token30.setToken(30);

        //token40
        token40.basico('t');
        token40.cerraduraEstrella();
        token40.setToken(40);


        //unirlos
        ArrayList<AFN> automatas = new ArrayList<AFN>();
        automatas.add(token10);
        automatas.add(token20);
        automatas.add(token30);
        automatas.add(token40);

        token10.unirAL(automatas);
        token10.imprimirTransicionesAFN();
        
*/
        //ejemplo del maestro para entregar
        /*
        AFN token10 = new AFN();
        AFN token20 = new AFN();
        AFN token30 = new AFN();
        AFN token40 = new AFN();

        token10.basico('a');
        b.basico('b');
        token10.unir(b);
        token10.cerraduraPositiva();
        c.basico('c');
        c.cerraduraEstrella();
        token10.concatenar(c);
        d.basico('d');
        d.cerraduraPositiva();
        token10.concatenar(d);
        token10.setToken(10);

        a=new AFN();
        b=new AFN();
        c=new AFN();
        d=new AFN();

        token20.basico('a');
        b.basico('b');
        b.cerraduraEstrella();
        token20.concatenar(b);
        c.basico('c');
        token20.concatenar(c);
        a.basico('a');
        a.cerraduraPositiva();
        token20.concatenar(a);
        token20.setToken(20);


        a=new AFN();
        b=new AFN();
        c=new AFN();
        d=new AFN();

        token30.basico('c');
        token30.cerraduraPositiva();
        b.basico('b');
        b.cerraduraPositiva();
        token30.concatenar(b);

        c.basico('c');
        d.basico('d');
        c.unir(d);
        c.cerraduraEstrella();
        token30.concatenar(c);
        token30.setToken(30);


        a=new AFN();
        b=new AFN();
        c=new AFN();
        d=new AFN();

        token40.basico('c');
        c.basico('c');
        token40.concatenar(c);
        c= new AFN();
        c.basico('c');
        token40.concatenar(c);
        b.basico('b');
        b.cerraduraPositiva();
        token40.concatenar(b);
        a.basico('a');
        a.cerraduraPositiva();
        d.basico('d');
        a.unir(d);
        token40.concatenar(a);
        token40.setToken(40);
        
        //unirlos
        ArrayList<AFN> automatas = new ArrayList<AFN>();
        automatas.add(token10);
        automatas.add(token20);
        automatas.add(token30);
        automatas.add(token40);

        token10.unirAL(automatas);
        token10.imprimirTransicionesAFN();
        */
        /*
        AFN token10 = new AFN(); //     +
        AFN token20 = new AFN(); //     -
        AFN token30 = new AFN(); //     *
        AFN token40 = new AFN(); //     /
        AFN token50 = new AFN(); //     ^
        AFN token60 = new AFN(); //     (
        AFN token70 = new AFN(); //     )
        AFN token80 = new AFN(); //     numero flotante
        AFN token90 = new AFN(); //     sin
        AFN token100 = new AFN(); //     cos
        AFN token110 = new AFN(); //     tan
        AFN token120 = new AFN(); //     exp
        AFN token130 = new AFN(); //     log
        AFN token140 = new AFN(); //     ln

        token10.basico('+');
        token10.setToken(10);
        token20.basico('-');
        token20.setToken(20);
        token30.basico('*');
        token30.setToken(30);
        token40.basico('/');
        token40.setToken(40);
        token50.basico('^');
        token50.setToken(50);

        token60.basico('(');
        token60.setToken(60);
        token70.basico(')');
        token70.setToken(70);

        //quito el +/-
        //token70.basico('+');
        //a.basico('-');
        //token70.unir(a);
        //token70.Question();



        //----------------------------------trigonometricas-------------------------------------------

        token80.basico('s');
        a = new AFN();
        a.basico('i');
        token80.concatenar(a);
        a = new AFN();
        a.basico('n');
        token80.concatenar(a);
        token80.setToken(80);

        token90.basico('c');
        a = new AFN();
        a.basico('o');
        token90.concatenar(a);
        a = new AFN();
        a.basico('s');
        token90.concatenar(a);
        token90.setToken(90);

        token100.basico('t');
        a = new AFN();
        a.basico('a');
        token100.concatenar(a);
        a = new AFN();
        a.basico('n');
        token100.concatenar(a);
        token100.setToken(100);

        token110.basico('e');
        a = new AFN();
        a.basico('x');
        token110.concatenar(a);
        a = new AFN();
        a.basico('p');
        token110.concatenar(a);
        token110.setToken(110);

        token120.basico('l');
        a = new AFN();
        a.basico('o');
        token120.concatenar(a);
        a = new AFN();
        a.basico('g');
        token120.concatenar(a);
        token120.setToken(120);

        token130.basico('l');
        a = new AFN();
        a.basico('n');
        token130.concatenar(a);
        token130.setToken(130);

        token140 = new AFN();
        token140.basico('0','9');
        token140.cerraduraPositiva();

        //token70.concatenar(num1);
        
        AFN punto = new AFN();
        punto.basico('.');

        AFN num1 = new AFN();
        num1.basico('0','9');
        num1.cerraduraPositiva();

        punto.concatenar(num1);
        punto.Question();

        token140.concatenar(punto);
        token140.setToken(140);

        //unirlos
        ArrayList<AFN> automatas = new ArrayList<AFN>();
        automatas.add(token10);
        automatas.add(token20);
        automatas.add(token30);
        automatas.add(token40);
        automatas.add(token50);
        automatas.add(token60);
        automatas.add(token70);
        automatas.add(token80);
        automatas.add(token90);
        automatas.add(token100);
        automatas.add(token110);
        automatas.add(token120);
        automatas.add(token130);
        automatas.add(token140);




        token10.unirAL(automatas);
        token10.imprimirTransicionesAFN();
        */


        AFN token10 = new AFN(); //     or
        AFN token20 = new AFN(); //     conc
        AFN token30 = new AFN(); //     cerr_pos
        AFN token40 = new AFN(); //     cerr_kleen
        AFN token50 = new AFN(); //     opc
        AFN token60 = new AFN(); //     (
        AFN token70 = new AFN(); //     )
        AFN token80 = new AFN(); //     simb
        AFN token90 = new AFN(); //     [
        AFN token100 = new AFN(); //     ]
        AFN token110 = new AFN(); //     -

        token90.basico('[');
        token90.setToken(90);
        token100.basico(']');
        token100.setToken(100);
        token110.basico('-');
        token110.setToken(110);

        token10.basico('|');
        token10.setToken(10);
        token20.basico('&');
        token20.setToken(20);
        token30.basico('+');
        token30.setToken(30);
        token40.basico('*');
        token40.setToken(40);
        token50.basico('?');
        token50.setToken(50);

        token60.basico('(');
        token60.setToken(60);
        token70.basico(')');
        token70.setToken(70);

        //quito el +/-
        //token70.basico('+');
        //a.basico('-');
        //token70.unir(a);
        //token70.Question();



        //----------------------------------simbolo-------------------------------------------

        token80.basico('\\');
        a = new AFN();
        a.basico('+');
        token80.concatenar(a);
        a = new AFN();
        b = new AFN();
        a.basico('\\');
        b.basico('*');
        a.concatenar(b);
        token80.unir(a);
        a = new AFN();
        b = new AFN();
        a.basico('\\');
        b.basico('?');
        a.concatenar(b);
        token80.unir(a);


        a = new AFN();
        b = new AFN();
        a.basico('\\');
        b.basico('(');
        a.concatenar(b);
        token80.unir(a);
        a = new AFN();
        b = new AFN();
        a.basico('\\');
        b.basico(')');
        a.concatenar(b);
        token80.unir(a);

        a = new AFN();
        b = new AFN();
        a.basico('\\');
        b.basico('(');
        a.concatenar(b);
        token80.unir(a);


        a = new AFN();
        b = new AFN();
        a.basico('\\');
        b.basico('-');
        a.concatenar(b);
        token80.unir(a);



        a = new AFN();
        b = new AFN();
        a.basico('\\');
        b.basico(')');
        a.concatenar(b);
        token80.unir(a);


        a = new AFN();
        a.basico('/');
        token80.unir(a);


        a = new AFN();
        a.basico('.');
        token80.unir(a);


        a = new AFN();
        a.basico(' ');
        token80.unir(a);

        a = new AFN();
        b = new AFN();
        a.basico('\\');
        b.basico('|');
        a.concatenar(b);
        token80.unir(a);
        a = new AFN();
        a.basico('a','z');
        token80.unir(a);
        a = new AFN();
        a.basico('A','Z');
        token80.unir(a);
        a = new AFN();
        a.basico('0','9');
        token80.unir(a);
        //token80.cerraduraPositiva();
        token80.setToken(80);

       
        //unirlos
        ArrayList<AFN> automatas = new ArrayList<AFN>();
        automatas.add(token10);
        automatas.add(token20);
        automatas.add(token30);
        automatas.add(token40);
        automatas.add(token50);
        automatas.add(token60);
        automatas.add(token70);
        automatas.add(token80);
        automatas.add(token90);
        automatas.add(token100);
        automatas.add(token110);

        token10.unirAL(automatas);
      //  token10.imprimirTransicionesAFN();


        //convertir a AFD
        AFD n = new AFD();
        n.convertirAFN(token10);
        int numeroInicial=token10.getInicial().getIdentificador();

        //n.imprimirTransicionesAFD();

        ArrayList<ArrayList<Integer>> tabla = n.tabla();
        ArrayList<Character> alfabetoOrdenado = n.getAlfabetoOrdenado();
        
        

       

        //para probar una cadena
        
        AnalizadorLexico al = new AnalizadorLexico();
        al.setAFD(n);
        //String cadena;
        //int tokaux=0;
        //CalculadoraCientifica calc = new CalculadoraCientifica(al);
        //String digito = "(\\+|\\-)&[0-9]+&(.&[0-9]+)?";
        //cadena = "("+digito+"&((\\*|/)&"+digito+")?)+";

        //lector de archivo
        AFD test_af = new AFD();
        lectorRegex lReg = new lectorRegex();
        lReg.setAnalizadorLexico(al);
        try{
            test_af = lReg.leerArchivo("a.txt");
        }catch(IOException e){};

        AnalizadorLexico new_al = new AnalizadorLexico(test_af);
        new_al.setCadena("-487.96+324   +0.44 a25-19");
        int res=0;
        while((res=new_al.getToken())!=1000 &&  res!=-1){
            System.out.println("Lexema:" +new_al.getLexema());
            System.out.println("Token:" +res+"\n");
        }
        
        //generador con expresiones regulares
        /*
        String a1 = "(\\+|\\-)?&[0-9]+";
        String a2 = "(\\+|\\-)?&[0-9]+&(.&[0-9]+)";
        String a3 = "[a-z]&([a-z]|[0-9])*";
        String a4 = "( )+";

        al.setCadena(a1);
        GeneradorAutomata gen = new GeneradorAutomata(al);



        AFN afn1 = gen.evaluar();
        afn1.setToken(10);
        al.setCadena(a2);
        AFN afn2 = gen.evaluar();
        afn2.setToken(20);
        
        al.setCadena(a3);
        AFN afn3 = gen.evaluar();
        afn3.setToken(30);
        
        al.setCadena(a4);
        AFN afn4 = gen.evaluar();
        afn4.setToken(40);


        ArrayList<AFN> listaAT = new ArrayList<AFN>();
        listaAT.add(afn1);
        listaAT.add(afn2);
        listaAT.add(afn3);
        listaAT.add(afn4);

        afn1.unirAL(listaAT);


        //afn1.unirAL(automatas);
        System.out.println("***************");
        AFD test_af = new AFD();
        test_af.convertirAFN(afn1);
        


        AnalizadorLexico new_al = new AnalizadorLexico(test_af);
        new_al.setCadena("-487.96+324   +0.44 a25-19");
        while(new_al.getToken()!=1000);
        */

        //System.out.println(new_al.cadenaUnica("3.3+2-3+3.21*432.1-21.2+31"));
    
        /*
        al.getToken();
        al.getToken();
        al.getToken();
        al.getToken();
        al.getToken();
        al.getToken();
        al.getToken();
        al.getToken();
        al.getToken();*/
        //gen.evaluar();
        /*
        //float num = calc.evaluar();
        //System.out.println("resultado: "+num);
        //try{
            //cadena = br.readLine();
            //al.setCadena(cadena);
            
            while((tokaux = al.getToken()) != 1000 && tokaux != -1) //el 0 es de termino -1 de error
                System.out.println(tokaux);

            System.out.println("fin");
            al.retrieveToken();
            al.retrieveToken();
            al.retrieveToken();
            System.out.println(al.getToken());
            System.out.println(al.getLexema());
            System.out.println(al.getToken());
            System.out.println(al.getLexema());
            System.out.println(al.getToken());
            System.out.println(al.getLexema());
            
        //}catch(IOException e){}
        */
    }   

}
