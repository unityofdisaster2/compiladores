Agregando transicion del estado 0: s -> 1
Agregando transicion del estado 2: 	[Epsilon] -> 0
Agregando transicion del estado 2: 	[Epsilon] -> 3
Agregando transicion del estado 1: 	[Epsilon] -> 3
Agregando transicion del estado 4: d -> 5
Agregando transicion del estado 6: 	[Epsilon] -> 4
Agregando transicion del estado 5: 	[Epsilon] -> 7
Agregando transicion del estado 5: 	[Epsilon] -> 4
Agregando siguientes transiciones a 3
	[Epsilon] -> 4
Agregando transicion del estado 8: s -> 9
Agregando transicion del estado 10: 	[Epsilon] -> 8
Agregando transicion del estado 10: 	[Epsilon] -> 11
Agregando transicion del estado 9: 	[Epsilon] -> 11
Agregando transicion del estado 12: d -> 13
Agregando transicion del estado 14: 	[Epsilon] -> 12
Agregando transicion del estado 13: 	[Epsilon] -> 15
Agregando transicion del estado 13: 	[Epsilon] -> 12
Agregando transicion del estado 16: . -> 17
Agregando transicion del estado 18: d -> 19
Agregando transicion del estado 20: 	[Epsilon] -> 18
Agregando transicion del estado 19: 	[Epsilon] -> 21
Agregando transicion del estado 19: 	[Epsilon] -> 18
Agregando siguientes transiciones a 11
	[Epsilon] -> 12
Agregando siguientes transiciones a 15
	. -> 17
Agregando siguientes transiciones a 17
	[Epsilon] -> 18
Agregando transicion del estado 22: l -> 23
Agregando transicion del estado 24: l -> 25
Agregando transicion del estado 26: d -> 27
Agregando transicion del estado 28: 	[Epsilon] -> 24
Agregando transicion del estado 28: 	[Epsilon] -> 26
Agregando transicion del estado 25: 	[Epsilon] -> 29
Agregando transicion del estado 27: 	[Epsilon] -> 29
Agregando transicion del estado 30: 	[Epsilon] -> 31
Agregando transicion del estado 30: 	[Epsilon] -> 28
Agregando transicion del estado 29: 	[Epsilon] -> 31
Agregando transicion del estado 29: 	[Epsilon] -> 28
Agregando siguientes transiciones a 23
	[Epsilon] -> 31
	[Epsilon] -> 28
Agregando transicion del estado 32: t -> 33
Agregando transicion del estado 34: 	[Epsilon] -> 35
Agregando transicion del estado 34: 	[Epsilon] -> 32
Agregando transicion del estado 33: 	[Epsilon] -> 35
Agregando transicion del estado 33: 	[Epsilon] -> 32
Agregando transicion del estado 36: 	[Epsilon] -> 2
Agregando transicion del estado 36: 	[Epsilon] -> 36
Agregando transicion del estado 36: 	[Epsilon] -> 10
Agregando transicion del estado 36: 	[Epsilon] -> 22
Agregando transicion del estado 36: 	[Epsilon] -> 34


Transiciones AFN
Transiciones nodo 36
	[Epsilon] -> 36
	[Epsilon] -> 2
	[Epsilon] -> 22
	[Epsilon] -> 34
	[Epsilon] -> 10
Transiciones nodo 33
	[Epsilon] -> 35
	[Epsilon] -> 32
Transiciones nodo 7
Transiciones nodo 5
	[Epsilon] -> 7
	[Epsilon] -> 4
Transiciones nodo 22
	l -> 23
Transiciones nodo 31
Transiciones nodo 25
	[Epsilon] -> 29
Transiciones nodo 27
	[Epsilon] -> 29
Transiciones nodo 29
	[Epsilon] -> 31
	[Epsilon] -> 28
Transiciones nodo 13
	[Epsilon] -> 15
	[Epsilon] -> 12
Transiciones nodo 35
Transiciones nodo 15
	. -> 17
Transiciones nodo 0
	s -> 1
Transiciones nodo 1
	[Epsilon] -> 3
Transiciones nodo 21
Transiciones nodo 32
	t -> 33
Transiciones nodo 9
	[Epsilon] -> 11
Transiciones nodo 19
	[Epsilon] -> 18
	[Epsilon] -> 21
Transiciones nodo 17
	[Epsilon] -> 18
Transiciones nodo 4
	d -> 5
Transiciones nodo 12
	d -> 13
Transiciones nodo 18
	d -> 19
Transiciones nodo 24
	l -> 25
Transiciones nodo 34
	[Epsilon] -> 35
	[Epsilon] -> 32
Transiciones nodo 23
	[Epsilon] -> 31
	[Epsilon] -> 28
Transiciones nodo 8
	s -> 9
Transiciones nodo 26
	d -> 27
Transiciones nodo 28
	[Epsilon] -> 24
	[Epsilon] -> 26
El inicial es de aceptacion
36
32
22
4
12
34
3
2
35
11
0
8
10
Con estado 37
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
s -> Nodo  1
Moviendo con s
s -> Nodo  9
Moviendo con s
3
11
9
1
4
12
No existe
Agregando transicion del estado 37: s -> 38
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
d -> Nodo  5
Moviendo con d
d -> Nodo  13
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
7
5
13
15
4
12
No existe
Agregando transicion del estado 37: d -> 39
Moviendo con t
Moviendo con t
t -> Nodo  33
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
33
35
32
No existe
Agregando transicion del estado 37: t -> 40
Moviendo con l
Moviendo con l
Moviendo con l
l -> Nodo  23
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
31
24
26
23
28
No existe
Agregando transicion del estado 37: l -> 41
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
vacio
Con estado 38
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
vacio
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
d -> Nodo  5
Moviendo con d
d -> Nodo  13
7
5
13
15
4
12
Ya existe
Agregando transicion del estado 38: d -> 39
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
vacio
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
vacio
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
vacio
Con estado 39
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
vacio
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
d -> Nodo  5
Moviendo con d
d -> Nodo  13
7
5
13
15
4
12
Ya existe
Agregando transicion del estado 39: d -> 39
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
vacio
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
vacio
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
. -> Nodo  17
Moviendo con .
Moviendo con .
17
18
No existe
Agregando transicion del estado 39: . -> 42
Con estado 41
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
vacio
Moviendo con d
Moviendo con d
Moviendo con d
d -> Nodo  27
Moviendo con d
Moviendo con d
29
31
27
24
26
28
No existe
Agregando transicion del estado 41: d -> 43
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
vacio
Moviendo con l
Moviendo con l
l -> Nodo  25
Moviendo con l
Moviendo con l
Moviendo con l
29
31
25
24
26
28
No existe
Agregando transicion del estado 41: l -> 44
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
vacio
Con estado 44
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
vacio
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
d -> Nodo  27
Moviendo con d
29
31
27
24
26
28
Ya existe
Agregando transicion del estado 44: d -> 43
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
vacio
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
l -> Nodo  25
Moviendo con l
Moviendo con l
29
31
25
24
26
28
Ya existe
Agregando transicion del estado 44: l -> 44
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
vacio
Con estado 40
Moviendo con s
Moviendo con s
Moviendo con s
vacio
Moviendo con d
Moviendo con d
Moviendo con d
vacio
Moviendo con t
Moviendo con t
Moviendo con t
t -> Nodo  33
33
35
32
Ya existe
Agregando transicion del estado 40: t -> 40
Moviendo con l
Moviendo con l
Moviendo con l
vacio
Moviendo con .
Moviendo con .
Moviendo con .
vacio
Con estado 43
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
Moviendo con s
vacio
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
Moviendo con d
d -> Nodo  27
Moviendo con d
29
31
27
24
26
28
Ya existe
Agregando transicion del estado 43: d -> 43
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
Moviendo con t
vacio
Moviendo con l
Moviendo con l
Moviendo con l
Moviendo con l
l -> Nodo  25
Moviendo con l
Moviendo con l
29
31
25
24
26
28
Ya existe
Agregando transicion del estado 43: l -> 44
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
Moviendo con .
vacio
Con estado 42
Moviendo con s
Moviendo con s
vacio
Moviendo con d
Moviendo con d
d -> Nodo  19
19
18
21
No existe
Agregando transicion del estado 42: d -> 45
Moviendo con t
Moviendo con t
vacio
Moviendo con l
Moviendo con l
vacio
Moviendo con .
Moviendo con .
vacio
Con estado 45
Moviendo con s
Moviendo con s
Moviendo con s
vacio
Moviendo con d
Moviendo con d
d -> Nodo  19
Moviendo con d
19
18
21
Ya existe
Agregando transicion del estado 45: d -> 45
Moviendo con t
Moviendo con t
Moviendo con t
vacio
Moviendo con l
Moviendo con l
Moviendo con l
vacio
Moviendo con .
Moviendo con .
Moviendo con .
vacio
----------------------------------------


Transiciones AFD
Transiciones nodo 42 final false
	d -> 45
Transiciones nodo 38 final false
	d -> 39
Transiciones nodo 45 final true
	d -> 45
Transiciones nodo 43 final true
	l -> 44
	d -> 43
Transiciones nodo 37 final true
	l -> 41
	t -> 40
	s -> 38
	d -> 39
Transiciones nodo 40 final true
	t -> 40
Transiciones nodo 41 final true
	l -> 44
	d -> 43
Transiciones nodo 39 final true
	. -> 42
	d -> 39
Transiciones nodo 44 final true
	l -> 44
	d -> 43
