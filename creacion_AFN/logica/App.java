package logica;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.LinkedList;



public class App {
    public static void main(String[] args) {

        GeneradorAnalizadores gen = new GeneradorAnalizadores();
        gen.setGeneradorAFN();
        
        //Gramaticas con posible recursividad a la izquierda
        AFN token10 = gen.generaAFN(gen.check("([A-Z]&[a-z]+)*&([A-Z])?|[A-Z]+|[a-z]+|=|^|!|\\+|\\-|\\*|/|\\(|\\)|\\&\\;| "));
        //gramaticas sin recursividad por la izquierda
        //AFN token10 = gen.generaAFN(gen.check("([A-Z]?&[a-z]+)*|[A-Z]|\\+|\\-|\\*|/|\\(|\\)|\\&\\;| "));
        token10.setToken(10);

        AFN token20 = gen.generaAFN(gen.check("\\-&>"));
        token20.setToken(20);

        AFN token30 = gen.generaAFN(gen.check(";"));
        token30.setToken(30);
        AFN token40 = gen.generaAFN(gen.check("\\|"));
        token40.setToken(40);
        AFN token50 = gen.generaAFN(gen.check("#"));
        token50.setToken(50);

        ArrayList<AFN> automatas = new ArrayList<AFN>();
        automatas.add(token10);
        automatas.add(token20);
        automatas.add(token30);
        automatas.add(token40);
        automatas.add(token50);
        token10.unirAL(automatas);

        AFD afd = new AFD();
        afd.convertirAFN(token10);

        AnalizadorLexico lex = new AnalizadorLexico();

        lex.setAFD(afd);

        
        String cadena = "";

        //File archivo = new File("logica\\gramatica.txt");
        File archivo = new File("logica\\extend_calc.txt");
        //File archivo = new File("logica\\calculadora.txt");
        //File archivo = new File("logica\\gramatica_libro.txt");
        try {
            Scanner lector = new Scanner(archivo);
            String cad_aux;
            while(lector.hasNextLine()){
                cad_aux = lector.nextLine();
                System.out.println(cad_aux);
                cadena += cad_aux;
                
            }
            lector.close();
        } catch (FileNotFoundException e) {
            //System.exit(1);
            e.printStackTrace();
        }

        cadena ="E->E+T|E^T|T;T->T*F|T/F|F;F->(E)|nu;";
        lex.setCadena(cadena);
        int token = 0;
        while (token != 1000) {
            token = lex.getToken();
            System.out.println(" token: " + token + " lexema: " + lex.getLexema());
        }



        lex.setCadena(cadena);
        GramaticaDeGramaticas gr = new GramaticaDeGramaticas(lex);
        ListaDeListas prueba = gr.analizar();

        System.out.println("LR0------");
        LR0 lr0 = new LR0(prueba);
        if(prueba != null){
            //lr0.leerCaracteres();
            lr0.tablaLR0();
            //System.out.println("\n\n\n"+lr0.analizarCadena("(nu+nu)^nu"));
        }

        // System.out.println("LR1------");
        // LR1 lr1 = new LR1(prueba);
        // if(prueba != null){
        //     lr1.tablaLR1();

        //     System.out.println("\n\n\n"+lr1.analizarCadena("(nu+nu)/nu^nu^nu"));
        // }

    }
}