package logica;

import java.util.*;
import java.io.*;
public class lectorRegex{

    private AnalizadorLexico al;
    private AFD afd;

    public lectorRegex(){
        al = null;
        afd = null;
    }

       
    public AFD leerArchivo(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        ArrayList<AFN> listaAT = new ArrayList<AFN>();
        GeneradorAutomata gen = new GeneradorAutomata(al);
        int x=0,token=0;
        AFN inicial = new AFN();
        AFN aux = new AFN();

        while((cadena = b.readLine())!=null) {
            al.setCadena(cadena);
            try{
                if(x%2==0){ //es regex
                    //System.out.println("."+cadena+".");
                    if(x==0){
                        inicial = gen.evaluar();
                    }else{
                        aux = gen.evaluar();
                    }
                }else{  //es token
                    token = Integer.parseInt(cadena);
                // System.out.println("-"+token+"-");

                    if(x==1){
                        inicial.setToken(token);
                        listaAT.add(inicial);
                    }else{
                        aux.setToken(token);
                        listaAT.add(aux);
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            x++;
        }

        if(x>=3){
            inicial.unirAL(listaAT);
        }

        b.close();

        afd = new AFD();
        afd.convertirAFN(inicial);
        return afd;
    }

    public void setAnalizadorLexico(AnalizadorLexico al){
        this.al = al;
    }

    public AFD getAFD(){
        return afd;
    }
}