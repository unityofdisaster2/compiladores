package logica;

import java.util.*;

public class mainLL1{
    public static void main(String[] args) {
        LL1 analizador = new LL1();
        /*
        analizador.setSInicial('S');
        analizador.agregarRegla('S',"ABCD");
        analizador.agregarRegla('A',"a|#");
        analizador.agregarRegla('B',"CD|b");
        analizador.agregarRegla('C',"c|#");
        analizador.agregarRegla('D',"Aa|d|#");
        */

        analizador.setSInicial('E');
        analizador.agregarRegla('E',"TR");
        analizador.agregarRegla('R',"+TR|#");
        analizador.agregarRegla('T',"FY");
        analizador.agregarRegla('Y',"*FY|#");
        analizador.agregarRegla('F',"(E)|i");

        HashSet<Character> noTerminal = analizador.getNoTerminales();

        for(Character c:noTerminal){
            
            System.out.println("\n\nFirst de "+c+":");
            Set<Character> a = analizador.first(c+"");

            for(Character e:a){
                System.out.print(e+" ");
            }
            
            System.out.println("\nFollow de "+c+":");

            a = analizador.follow(c);
            
            for(Character e:a){
                System.out.print(e+" ");
            }
        }
    }
}